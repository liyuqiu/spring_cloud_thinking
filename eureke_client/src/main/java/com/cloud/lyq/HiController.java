package com.cloud.lyq;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiController {

    @Value("${server.port}")
    private String port;


    @GetMapping("/hi")
    public String hi(@RequestParam String name)throws Exception{
//        int i =1/0;
        return "port="+port+" name="+name;
    }

    @GetMapping("/hi2")
    public String hi(@RequestParam String name,@RequestParam String age)throws Exception{
//        int i =1/0;
        return "port="+port+" name="+name+"age="+age;
    }
}
