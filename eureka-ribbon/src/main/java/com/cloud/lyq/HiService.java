package com.cloud.lyq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HiService {


    @LoadBalanced
    @Autowired
    private RestTemplate restTemplate;


    public String hi(String name) {
        return restTemplate.getForObject("http://service-hi/hi?name=" + name, String.class);
    }
}
