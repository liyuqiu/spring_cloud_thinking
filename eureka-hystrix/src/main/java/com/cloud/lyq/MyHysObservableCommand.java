package com.cloud.lyq;

import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixObservableCommand;
import rx.Observable;

public class MyHysObservableCommand extends HystrixObservableCommand {

    protected MyHysObservableCommand(HystrixCommandGroupKey group) {
        super(group);
    }

    @Override
    protected Observable construct() {
        return null;
    }

    //事件注册前执行run()/construct()，支持接收多个值对象，取决于发射源。
    //调用observe()会返回一个hot Observable，也就是说，调用observe()自动触发执行run()/construct()，无论是否存在订阅者。
    //如果继承的是HystrixCommand，hystrix会从线程池中取一个线程以非阻塞方式执行run()；
    //如果继承的是HystrixObservableCommand，将以调用线程阻塞执行construct()。

    /**
     * observe()使用方法：
     * 调用observe()会返回一个Observable对象
     * 调用这个Observable对象的subscribe()方法完成事件注册，从而获取结果
     */
    @Override
    public Observable observe() {
        return super.observe();
    }

    @Override
    public Observable toObservable() {
        return super.toObservable();
    }
}
