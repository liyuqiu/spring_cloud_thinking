package com.cloud.lyq;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HiService {

    @LoadBalanced
    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(threadPoolKey = "hi",fallbackMethod = "fallbackService")
    public String hi(String name) {
        return restTemplate.getForObject("http://service-hi/hi?name=" + name, String.class);
    }



    public  String fallbackService(String name) {
        System.out.println(1111);
        return "调用的服务不存在！";
    }


}
