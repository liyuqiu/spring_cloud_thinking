package com.cloud.lyq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiController {

    @Autowired
    private HiService hiService;

    @Autowired
    private HiClientFallBack hiClient;

    @GetMapping(value = "/hi")
    public String hi(@RequestParam String name) {
        return hiService.hi(name);
    }


}
