package com.cloud.lyq;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "service-hi", fallback = HiClientFallBack.class)
public interface HiClient {

    @GetMapping("/hi2")
    String hi(String name);


}
