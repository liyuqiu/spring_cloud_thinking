package com.cloud.lyq;

import org.springframework.stereotype.Component;

@Component
public class HiClientFallBack extends HiService{

    @Override
    public String hi(String name) {
        System.out.println( "你好："+name);
        return "服务调用失败,降级";
    }
}
