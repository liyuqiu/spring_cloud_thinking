package com.cloud.lyq;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class SpringCloudThinkingApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudThinkingApp.class,args);
    }
}
