package com.cloud.lyq;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "service-hi" ,fallback = HiServiceFallBack.class)
public interface HiService {

    @GetMapping("/hi")
    String hi(@RequestParam  String name);
}
