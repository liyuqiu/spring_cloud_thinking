package com.cloud.lyq;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitConfigApplication.class,args);
    }
}
